import logging
import os
import re
from os import listdir
from os.path import isfile, join

import jsonlines
from educationenrichoccupations import settings


class LocalFileDownloader(object):
    currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'
    ad_src_filepath = currentdir + settings.AD_SRC_RELATIVE_LOCAL_FILES_PATH



    def __init__(self):
        logging.basicConfig(level=logging.INFO)
        self.log = logging.getLogger(__name__)


    def download_ads_from_file(self, ad_src_file_name):
        filepath = f"{self.ad_src_filepath}{ad_src_file_name}"
        self.log.info(f"Returning ads from file {filepath}")

        with jsonlines.open(filepath) as reader:
            for ad in reader.iter(type=dict, skip_invalid=True):
                yield ad


    def get_filtered_filenames(self):
        files = [f for f in listdir(self.ad_src_filepath) if isfile(join(self.ad_src_filepath, f))]
        RE_WANTED_FILE_PATTERN = re.compile(settings.S3_AD_SRC_FILE_NAME_PATTERN, re.UNICODE)
        filtered_filenames = [filename for filename in files if
                              RE_WANTED_FILE_PATTERN.match(filename)]

        return filtered_filenames

