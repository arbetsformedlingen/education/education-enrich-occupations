import logging

import jmespath
import requests

from educationenrichoccupations import settings
from educationenrichoccupations import taxonomy_queries

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)


class TaxonomyClient(object):
    taxonomy_occupations = {}
    occupation_id_to_occupation = {}

    current_taxonomy_occupations = {}
    current_taxonomy_occupation_ids = set()

    def __init__(self):
        self.taxonomy_occupations = self.load_taxonomy_occupations()
        self.current_taxonomy_occupations = self.load_current_taxonomy_occupations()

    def load_taxonomy_occupations(self):
        if self.taxonomy_occupations:
            # Cache, avoid multiple calls to taxonomy endpoint
            return self.taxonomy_occupations

        params = {'query': taxonomy_queries.TAXONOMY_OCCUPATIONS_QUERY}
        taxonomy_response = requests.get(url=settings.TAXONOMY_GRAPHQL_URL, headers={}, params=params)
        taxonomy_response.raise_for_status()
        self.taxonomy_occupations = jmespath.search('data.concepts', taxonomy_response.json())

        return self.taxonomy_occupations

    def load_occupation_id_to_taxonomy_occupation(self):
        if self.occupation_id_to_occupation:
            return self.occupation_id_to_occupation

        taxonomy_occupations = self.load_taxonomy_occupations()

        occupation_id_to_occupation = {}

        for tax_occupation in taxonomy_occupations:
            occupation_id = jmespath.search('id', tax_occupation)

            tax_occupation_group = jmespath.search('broader | [0]', tax_occupation)

            tax_replaced_by = jmespath.search('replaced_by | [0]', tax_occupation)
            if not tax_replaced_by:
                replaced_by = {}
            else:
                replaced_by = {
                    "concept_id": jmespath.search('id', tax_replaced_by),
                    "label": jmespath.search('preferred_label', tax_replaced_by)
                }

            if not tax_occupation_group and not replaced_by:
                # Occupation has no occupation_group and no replaced_by, don't add the occupation to return value
                continue

            if not tax_occupation_group:
                occupation_group = {}
            else:
                occupation_group = {
                    "concept_id": jmespath.search('id', tax_occupation_group),
                    "label": jmespath.search('preferred_label', tax_occupation_group),
                    "legacy_ams_taxonomy_id": jmespath.search('ssyk_code_2012', tax_occupation_group)
                }

            converted_tax_occupation = {
                "concept_id": occupation_id,
                "label": jmespath.search('preferred_label', tax_occupation),
                "legacy_ams_taxonomy_id": jmespath.search('deprecated_legacy_id', tax_occupation),
                "occupation_group": occupation_group,
                "replaced_by": replaced_by
            }
            self.occupation_id_to_occupation[occupation_id] = converted_tax_occupation

        log.info(f'Found {len(self.occupation_id_to_occupation)} occupations in Taxonomy v{settings.TAXONOMY_VERSION}')

        return self.occupation_id_to_occupation

    def load_current_taxonomy_occupations(self):
        if self.current_taxonomy_occupations:
            # Cache, avoid multiple calls to taxonomy endpoint
            return self.current_taxonomy_occupations

        params = {'query': taxonomy_queries.CURRENT_TAXONOMY_OCCUPATIONS_QUERY}
        taxonomy_response = requests.get(url=settings.TAXONOMY_GRAPHQL_URL, headers={}, params=params)
        taxonomy_response.raise_for_status()
        self.current_taxonomy_occupations = jmespath.search('data.concepts', taxonomy_response.json())

        return self.current_taxonomy_occupations

    def load_current_taxonomy_occupation_ids(self):
        if self.current_taxonomy_occupation_ids:
            return self.current_taxonomy_occupation_ids

        current_taxonomy_occupations = self.load_current_taxonomy_occupations()

        for tax_occupation in current_taxonomy_occupations:
            occupation_id = jmespath.search('id', tax_occupation)
            self.current_taxonomy_occupation_ids.add(occupation_id)

        return self.current_taxonomy_occupation_ids
