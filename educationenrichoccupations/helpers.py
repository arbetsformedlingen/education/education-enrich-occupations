import gzip
import itertools
import json
import logging
import os

from langdetect import detect
from langdetect.lang_detect_exception import LangDetectException

from educationenrichoccupations import settings

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'
FILEPATH_JOBAD_ID_2_LANGCODE = currentdir + settings.LANG_CODES_CACHE_RELATIVE_FILE_PATH


def load_json_file(filepath):
    log.info('Loading json from file: %s' % filepath)
    with open(filepath, 'r', encoding='utf-8') as file:
        data = json.load(file)
        return data


def grouper(n, iterable):
    iterable = iter(iterable)
    return iter(lambda: list(itertools.islice(iterable, n)), [])


def detect_langcode(text_for_detection):
    if not text_for_detection:
        return "xx"

    # Note: If text to detect is in uppercase, the langdetect function might predict the language to be german instead of (correct) swedish.
    text_for_detection = text_for_detection.lower()
    try:
        lang_code = detect(text_for_detection)
        return lang_code
    except LangDetectException:
        # Can't detect lang_code
        return "xy"

def load_json_gz_file(filepath):
    log.info('Loading cached langcodes from file: %s' % filepath)
    with gzip.open(filepath, 'rt', encoding='utf-8') as f:
        data = json.load(f)
        return data

def load_cached_langcodes():
    filepath = FILEPATH_JOBAD_ID_2_LANGCODE
    if not os.path.exists(filepath):
        log.info('File for cached langcodes does not exist')
        return {}

    cached_lang_codes = load_json_gz_file(filepath)
    log.info('Loaded %s cached langcodes' % len(cached_lang_codes))

    return cached_lang_codes

def save_json_gz_file(data, filepath):
    log.info('Saving cached langcodes to file: %s' % filepath)
    with gzip.open(filepath, 'wt', encoding='utf-8') as f:
        json.dump(data, f)

def save_cached_langcodes(cached_langcodes) -> None:
    save_json_gz_file(cached_langcodes, FILEPATH_JOBAD_ID_2_LANGCODE)
