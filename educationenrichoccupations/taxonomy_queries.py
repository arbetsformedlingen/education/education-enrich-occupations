from educationenrichoccupations import settings


_taxonomy_occupations_query = """
query OccupationsQuery {
  concepts(type: "occupation-name", include_deprecated: true, version: %s) {
    type
    id
    preferred_label
    deprecated_legacy_id
    broader(type: "ssyk-level-4") {
      type
      id
      preferred_label
      ssyk_code_2012
      broader(type: "occupation-field") {
        type
        id
        preferred_label
        deprecated_legacy_id
      }
    }
    replaces {
      id
      preferred_label
      deprecated_legacy_id
    }    
    replaced_by(include_deprecated: true) {
      type
      id
      preferred_label
      deprecated_legacy_id
      broader(type: "ssyk-level-4"){
        id
        type
        preferred_label
        ssyk_code_2012
        broader(type: "occupation-field") {
          type
          id
          preferred_label
          deprecated_legacy_id
        }
      }
    }
  }
}
"""
TAXONOMY_OCCUPATIONS_QUERY = _taxonomy_occupations_query % settings.TAXONOMY_VERSION

_current_taxonomy_occupations_query = """
query CurrentOccupationsQuery {
  concepts(type: "occupation-name", include_deprecated: false, version: %s) {
    type
    id
    preferred_label
    deprecated_legacy_id
  }
}
"""
CURRENT_TAXONOMY_OCCUPATIONS_QUERY = _current_taxonomy_occupations_query % settings.TAXONOMY_VERSION