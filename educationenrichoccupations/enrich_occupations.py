import concurrent.futures
import copy
import json
import logging
import os
import sys
import time
from collections import Counter
from multiprocessing import Value

import jmespath
import requests
from education_opensearch.opensearch_store import OpensearchStore

from educationenrichoccupations import settings
from educationenrichoccupations.decorators import timeit
from educationenrichoccupations.helpers import grouper, detect_langcode, load_cached_langcodes, save_cached_langcodes
from educationenrichoccupations.local_file_downloader import LocalFileDownloader
from educationenrichoccupations.minio_downloader import MinioDownloader
from educationenrichoccupations.taxonomy_client import TaxonomyClient

PERCENT_TRAITS_FOR_OCC_THRESHOLD = 0.1

PERCENT_JOBTITLE_FOR_OCC_THRESHOLD = 10.0

PERCENT_COMPETENCE_FOR_OCC_THRESHOLD = 0.05

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'

counter = Value('i', 0)
cached_langcodes = {}

enriched_aggregated_occupations = {}

ENRICHER_PARAM_DOC_ID = 'doc_id'
ENRICHER_PARAM_DOC_HEADLINE = 'doc_headline'
ENRICHER_PARAM_DOC_TEXT = 'doc_text'
ENRICHER_RETRIES = 10

taxonomy_client = TaxonomyClient()

occupation_id_to_taxonomy_occupation = taxonomy_client.load_occupation_id_to_taxonomy_occupation()
current_taxonomy_occupation_ids = taxonomy_client.load_current_taxonomy_occupation_ids()


def get_enrich_result(batch_indata, jae_api_endpoint_url, timeout):
    log.info('Getting result from endpoint: %s' % jae_api_endpoint_url)

    headers = {'Content-Type': 'application/json'}

    for retry in range(ENRICHER_RETRIES):
        try:
            r = requests.post(url=jae_api_endpoint_url, headers=headers, json=batch_indata, timeout=timeout)
            r.raise_for_status()
        except Exception as e:
            log.error(f"get_enrich_result() retrying #{retry + 1} after error: {e}")
            time.sleep(0.5)
        else:
            return r.json()
    log.error(f"_get_enrich_result failed after: {ENRICHER_RETRIES} retries with error. Exit!")
    sys.exit(1)


def execute_calls(batch_indatas, jae_api_endpoint_url, parallelism):
    global counter
    enriched_output = {}
    # We can use a with statement to ensure threads are cleaned up promptly
    with concurrent.futures.ThreadPoolExecutor(max_workers=parallelism) as executor:
        # Start the load operations and mark each future with its URL
        future_to_enrich_result = {executor.submit(get_enrich_result, batch_indata, jae_api_endpoint_url,
                                                   settings.JAE_API_TIMEOUT_SECONDS): batch_indata
                                   for batch_indata in batch_indatas}
        for future in concurrent.futures.as_completed(future_to_enrich_result):
            try:
                enriched_result = future.result()
                for resultrow in enriched_result:
                    enriched_output[resultrow[ENRICHER_PARAM_DOC_ID]] = resultrow
                    with counter.get_lock():
                        counter.value += 1
                        if counter.value % 1000 == 0:
                            log.info(f'enrichtextdocuments - Processed docs: {counter.value}')
            except requests.exceptions.JSONDecodeError as e:
                log.error(f"Error in _execute_calls: {e}")
            except Exception as e:
                log.error(e)
                raise

    return enriched_output


def enrich(ad_batches, batch_indata_config, endpoint_url, parallelism):
    batch_indatas = []
    for i, ad_batch in enumerate(ad_batches):
        ad_batch_indatas = [ad_indata for ad_indata in ad_batch]
        batch_indata = copy.deepcopy(batch_indata_config)
        batch_indata['documents_input'] = ad_batch_indatas
        batch_indatas.append(batch_indata)
    log.info('Enriching with nr of batches: %s' % len(batch_indatas))
    enrich_results_data = execute_calls(batch_indatas, endpoint_url, parallelism)

    return enrich_results_data


def get_empty_output_value():
    return {
        "enriched_candidates": {
            "occupations": [],
            "competencies": [],
            "traits": [],
            "geos": []
        }
    }


def get_null_safe_value(element, key, replacement_val):
    val = element.get(key, replacement_val)
    if val is None:
        val = replacement_val
    return val


def add_if_any_value(value, headline_parts):
    if value:
        headline_parts.append(value)


def get_doc_headline_input(jobad):
    sep = ' | '
    headline_parts = []

    occupation_from_ad = get_occupation_from_ad(jobad)
    occupation_label = jmespath.search("label", occupation_from_ad)
    add_if_any_value(occupation_label, headline_parts)

    wp_address_node = jmespath.search("workplace_address", jobad)

    if wp_address_node:
        add_if_any_value(get_null_safe_value(wp_address_node, 'city', ''), headline_parts)
        add_if_any_value(get_null_safe_value(wp_address_node, 'municipality', ''), headline_parts)
        add_if_any_value(get_null_safe_value(wp_address_node, 'region', ''), headline_parts)
        add_if_any_value(get_null_safe_value(wp_address_node, 'country', ''), headline_parts)

    add_if_any_value(get_null_safe_value(jobad, 'headline', ''), headline_parts)

    doc_headline_input = sep.join(headline_parts)

    return doc_headline_input

@timeit
def prepare_enrich_input(jobads):
    global cached_langcodes
    total_jobads_size = len(jobads)
    nr_of_items_per_batch = 100
    # Prepare input
    ads_input_data = []
    input_ad_counter = 0
    added_ad_ids = set()
    duplicate_ids = []
    removed_ids = []
    jobad_id_lang_code = {}

    skipped_occupations = set()

    for jobad in jobads:
        doc_id = str(jobad.get('id', ''))

        is_removed = jobad.get('removed', False)

        if is_removed:
            # print('jobad id %s is removed!' % doc_id)
            removed_ids.append(doc_id)
            continue

        if doc_id in added_ad_ids:
            # Skip duplicate ids.
            duplicate_ids.append(doc_id)
            continue
        else:
            added_ad_ids.add(doc_id)

        occupation = get_replaced_by_occupation_or_original(jobad)
        # Make sure that the occupation.concept_id is valid (from the wanted Taxonomy version).
        occupation_id = jmespath.search("concept_id", occupation)
        if occupation_id not in current_taxonomy_occupation_ids:
            skipped_occupations.add(json.dumps(occupation))
            continue

        jobad['occupation'] = occupation

        doc_headline = get_doc_headline_input(jobad)
        doc_text = jobad.get('description', {}).get('text_formatted', '')

        if not doc_text:
            doc_text = jobad.get('description', {}).get('text', '')

        if doc_id in cached_langcodes:
            detected_lang_code = cached_langcodes[doc_id]
        else:
            # Check language, only enrich for swedish ads.
            text_for_detection = None
            if doc_text:
                text_for_detection = doc_text.strip()[:400]

            detected_lang_code = detect_langcode(text_for_detection)
            # log.info('Detected lang_code for ad id %s: %s' % (doc_id, detected_lang_code))

        jobad_id_lang_code[doc_id] = detected_lang_code
        # print('Detected langcode: %s' % detected_lang_code)

        if 'sv' == detected_lang_code:
            input_doc_params = {
                ENRICHER_PARAM_DOC_ID: doc_id,
                ENRICHER_PARAM_DOC_HEADLINE: doc_headline,
                ENRICHER_PARAM_DOC_TEXT: doc_text
            }

            ads_input_data.append(input_doc_params)

        input_ad_counter += 1
        if input_ad_counter % 1000 == 0:
            log.info(
                f'Prepared input for {input_ad_counter} ads. {len(ads_input_data)}/{input_ad_counter} are written in swedish.')

    for key in jobad_id_lang_code.keys():
        if key not in cached_langcodes:
            cached_langcodes[key] = jobad_id_lang_code[key]

    save_cached_langcodes(cached_langcodes)

    log.debug(f'Found {len(duplicate_ids)} duplicate ids in {total_jobads_size} jobads')
    log.debug(f'Found {len(removed_ids)} removed ids in {total_jobads_size} jobads')

    log.info('Prepared input to enrich %s/%s jobads' % (len(ads_input_data), total_jobads_size))
    ad_batches = grouper(nr_of_items_per_batch, ads_input_data)
    if len(skipped_occupations) > 0:
        log.warning(f"Skipped occupations that could not be found in Taxonomy after trying to convert (with replaced_by) to current occupations: {skipped_occupations}")

    return ad_batches


def add_enriched_result(items_to_update, enriched_results_data):
    for item_to_update in items_to_update:
        if not 'text_enrichments_results' in item_to_update:
            item_to_update['text_enrichments_results'] = {}
        doc_id = str(item_to_update.get('id', ''))
        if doc_id in enriched_results_data:
            enriched_output = enriched_results_data[doc_id]
            enriched_output.pop(ENRICHER_PARAM_DOC_ID, None)
            enriched_output.pop(ENRICHER_PARAM_DOC_HEADLINE, None)
        else:
            # Set non valid ads to empty enriched values.
            enriched_output = get_empty_output_value()
        # print('Setting enriched output for item to: %s ' % enriched_output)
        item_to_update['text_enrichments_results'] = enriched_output


def format_concept_labels(enriched_candidates):
    return [candidate['concept_label'].lower() for candidate in enriched_candidates]


def enrich_occupations():
    if settings.AD_SRC_USE_LOCAL_FILES:
        local_file_downloader = LocalFileDownloader()
        ad_src_file_names = local_file_downloader.get_filtered_filenames()
        log.info(f'Will use local files in {settings.AD_SRC_RELATIVE_LOCAL_FILES_PATH} '
                 f'and enrich from the following files:\n{ad_src_file_names}')
        downloader = local_file_downloader
    else:
        minio_downloader = MinioDownloader()
        ad_src_file_names = minio_downloader.get_filtered_filenames_in_bucket()
        log.info('Will download from bucket %s and enrich from the following files:\n%s' % (
            settings.S3_BUCKET_NAME, ad_src_file_names))
        downloader = minio_downloader

    ad_counter = 0

    skipped_occupations = set()

    for ad_src_file_name in ad_src_file_names:
        ad_items_to_enrich = []

        skipped_ads = []
        for ad in downloader.download_ads_from_file(ad_src_file_name):
            # Exlude ads that have a deprecated occupation in Taxonomy
            occupation = get_occupation_from_ad(ad)

            occupation_id = jmespath.search("concept_id", occupation)

            if not occupation_id in occupation_id_to_taxonomy_occupation:
                skipped_ads.append(ad)
                skipped_occupations.add(json.dumps(occupation))
                continue

            # Remove enriched values.
            if 'keywords' in ad:
                ad.pop('keywords', None)

            # ad_stream_items.append(ad)
            ad_items_to_enrich.append(copy.deepcopy(ad))
            ad_counter += 1
            if ad_counter % 10000 == 0:
                log.info('Retrieving ads, so far: %s' % ad_counter)

        enrich_and_append_to_occupations(ad_items_to_enrich)

        if len(skipped_ads) > 0:
            log.warning(f'Skipping {len(skipped_ads)} ads since the occupation cant be found in Taxonomy')
    if len(skipped_occupations) > 0:
        log.warning(f"Skipped occupations that could not be found in Taxonomy: {skipped_occupations}")


def get_occupation_from_ad(ad):
    occupation_path = jmespath.search("occupation", ad)
    occupation = {}
    if isinstance(occupation_path, dict):
        occupation = occupation_path
    elif isinstance(occupation_path, list):
        occupation = jmespath.search("occupation[0]", ad)
    else:
        log.warning(f"Could not find occupation in ad {ad['id']}")
    return occupation


def enrich_and_append_to_occupations(ad_items_to_enrich):
    parallelism = settings.ENRICH_CALLS_PARALLELISM

    if parallelism <= 0:
        parallelism = 1
    ad_batches = prepare_enrich_input(ad_items_to_enrich)
    batch_indata_config = {
        "include_terms_info": True,
        "include_sentences": False,
        "include_synonyms": False,
        "include_misspelled_synonyms": False
    }
    enrich_results = enrich(ad_batches, batch_indata_config, settings.JAE_API_URL + '/enrichtextdocumentsbinary',
                            parallelism)
    add_enriched_result(ad_items_to_enrich, enrich_results)
    extend_enriched_data_for_occupations(enriched_aggregated_occupations, ad_items_to_enrich)


def extend_enriched_data_for_occupations(enriched_aggrated_occupations, enriched_jobads):


    for enriched_jobad in enriched_jobads:
        occupation = get_replaced_by_occupation_or_original(enriched_jobad)

        occupation_label = jmespath.search("label", occupation)

        if occupation_label:
            enriched_candidates = jmespath.search('text_enrichments_results.enriched_candidates', enriched_jobad)
            enriched_competencies = format_concept_labels(jmespath.search('competencies', enriched_candidates))
            enriched_geos = format_concept_labels(jmespath.search('geos', enriched_candidates))
            enriched_occupations = format_concept_labels(jmespath.search('occupations', enriched_candidates))
            enriched_traits = format_concept_labels(jmespath.search('traits', enriched_candidates))

            if not occupation_label in enriched_aggrated_occupations:
                enriched_aggrated_occupations[occupation_label] = create_occupation_node(occupation)

            enriched_occupation_node = enriched_aggrated_occupations[occupation_label]['occupation']

            enriched_occupation_node["enriched_ads_count"] = enriched_occupation_node["enriched_ads_count"] + 1
            candidates_node = enriched_occupation_node['enriched_candidates']
            candidates_node['competencies'].extend(enriched_competencies)
            candidates_node['geos'].extend(enriched_geos)
            candidates_node['occupations'].extend(enriched_occupations)
            candidates_node['traits'].extend(enriched_traits)


def get_replaced_by_occupation_or_original(enriched_jobad):
    occupation_from_ad = get_occupation_from_ad(enriched_jobad)
    occupation_id_from_ad = jmespath.search("concept_id", occupation_from_ad)

    valid_occupation = get_replaced_by_occupation_recursive_or_original(occupation_id_from_ad)
    return valid_occupation

def get_replaced_by_occupation_recursive_or_original(input_occupation_id):
    occupation = occupation_id_to_taxonomy_occupation[input_occupation_id]
    # Get eventual replaced_by from occupation_id_to_taxonomy_occupation
    # (a newer occupation that replaces the one in the ad)
    occupation_replaced_by = jmespath.search('replaced_by', occupation)
    if occupation_replaced_by:
        occupation_replaced_by_id = jmespath.search('concept_id', occupation_replaced_by)
        if occupation_replaced_by_id != input_occupation_id:
            return get_replaced_by_occupation_recursive_or_original(occupation_replaced_by_id)
        else:
            # Info: Some of the Taxonomy occupations have bad data, where replaced_by has the same id as the occupation
            # concept_id, for example: "concept_id": "Wmuh_RXD_cv5", "label": "Grafisk operatör, digital produktion".
            # Without the check if the id:s are different, this function can lead to a RecursionError.
            return occupation
    return occupation


def create_occupation_node(occupation):
    occupation_id = jmespath.search("concept_id", occupation)
    return {
        "id": occupation_id,
        "occupation": {
            "enriched_ads_count": 0,
            "concept_id": occupation_id,
            "label": jmespath.search('label', occupation),
            "legacy_ams_taxonomy_id": jmespath.search("legacy_ams_taxonomy_id", occupation),
            "occupation_group": {
                "concept_id": jmespath.search("occupation_group.concept_id", occupation),
                "label": jmespath.search("occupation_group.label", occupation),
                "legacy_ams_taxonomy_id": jmespath.search("occupation_group.legacy_ams_taxonomy_id", occupation)
            },
            'enriched_candidates': {'competencies': [],
                                    'geos': [],
                                    'occupations': [],
                                    'traits': []
                                    }

        }
    }


def save_enriched_occupations_in_repository(enriched_occupations_to_store):
    os_store = OpensearchStore()

    index_name = os_store.start_new_save(settings.OCCUPATIONS_ALIAS_NAME,
                                         mappings=settings.ENRICHED_OCCUPATIONS_INDEX_CONFIG)

    log.info('Will save enriched occupations in index_name: %s' % index_name)

    os_store.save_items_in_repository(enriched_occupations_to_store)
    os_store.create_or_update_alias_for_index(index_name, settings.OCCUPATIONS_ALIAS_NAME)


def write_items_to_file(json_src, filepath):
    file = open(filepath, 'w', encoding='utf-8')
    for record in json_src:
        file.write(json.dumps(record) + '\n')
    file.close()


def calculate_term_frequency(enriched_terms, include_percent=True):
    if not enriched_terms:
        return {}
    terms_count = len(enriched_terms)
    most_common_terms = Counter(enriched_terms).most_common()

    log.debug('Total number of enriched words: %s' % (terms_count))
    unique_enriched_terms = len(set(enriched_terms))
    log.debug('Number of unique enriched words: %s' % (unique_enriched_terms))

    terms_and_percent = []
    for occ_tuple in most_common_terms:
        unique_term = occ_tuple[0]
        unique_term_count = occ_tuple[1]

        percent_unique_term = (unique_term_count / terms_count) * 100
        if include_percent:
            terms_and_percent.append({
                "term": unique_term,
                "percent_for_occupation": percent_unique_term
            })
        else:
            terms_and_percent.append(unique_term)
    return terms_and_percent


def filter_and_add_relevant_enriched_candidates(enriched_occupations_to_store):
    '''
    Filters the most frequent candidates and adds them to a separate attribute.
    :param enriched_occupations_to_store: enriched occupations
    :return: enriched occupations with a new attribute with the most frequent candidates.
    '''
    for enriched_occupation_to_store in enriched_occupations_to_store:
        if enriched_occupation_to_store.get('occupation'):
            occupation = enriched_occupation_to_store['occupation']
            relevant_enriched_candidates = {}
            if occupation.get('enriched_candidates'):
                enriched_candidates = occupation['enriched_candidates']
                if enriched_candidates.get('competencies'):
                    complete_competencies = enriched_candidates['competencies']
                    competencies_frequency = calculate_term_frequency(complete_competencies, include_percent=True)
                    competencies_above_threshold = [item['term'] for item in competencies_frequency if
                                                    item['percent_for_occupation'] >= PERCENT_COMPETENCE_FOR_OCC_THRESHOLD]

                    all_competencies_above_threshold = [item for item in complete_competencies if
                                                        item in set(competencies_above_threshold)]

                    relevant_enriched_candidates['competencies'] = all_competencies_above_threshold
                if enriched_candidates.get('occupations'):
                    complete_occupations = enriched_candidates['occupations']
                    occupations_frequency = calculate_term_frequency(complete_occupations, include_percent=True)
                    occupations_above_threshold = [item['term'] for item in occupations_frequency if
                                                   item['percent_for_occupation'] >= PERCENT_JOBTITLE_FOR_OCC_THRESHOLD]
                    occupations_above_threshold = occupations_above_threshold[:3]
                    all_occupations_above_threshold = [item for item in complete_occupations if
                                                       item in set(occupations_above_threshold)]

                    relevant_enriched_candidates['occupations'] = all_occupations_above_threshold
                if enriched_candidates.get('geos'):
                    complete_geos = enriched_candidates['geos']
                    geos_frequency = calculate_term_frequency(complete_geos, False)
                    relevant_geos_from_frequency = geos_frequency[:100]

                    relevant_enriched_candidates['geos'] = relevant_geos_from_frequency
                if enriched_candidates.get('traits'):
                    complete_traits = enriched_candidates['traits']
                    traits_frequency = calculate_term_frequency(complete_traits, include_percent=True)
                    traits_above_threshold = [item['term'] for item in traits_frequency if
                                              item['percent_for_occupation'] >= PERCENT_TRAITS_FOR_OCC_THRESHOLD]
                    all_traits_above_threshold = [item for item in complete_traits if
                                                  item in set(traits_above_threshold)]

                    relevant_enriched_candidates['traits'] = all_traits_above_threshold
            occupation['relevant_enriched_candidates'] = relevant_enriched_candidates
    return enriched_occupations_to_store

@timeit
def enrich_occupations_and_persist():
    global enriched_aggregated_occupations
    global cached_langcodes

    if settings.OCCUPATIONS_SAVE_RESULT_IN_FILE_ONLY:
        if not settings.OCCUPATIONS_RELATIVE_FILE_PATH:
            raise RuntimeError('Env variable OCCUPATIONS_SAVE_RESULT_IN_FILE_ONLY is True but OCCUPATIONS_RELATIVE_FILE_PATH has not been set.')
        log.info('Will save the result in file only, with filepath: %s' % currentdir + settings.OCCUPATIONS_RELATIVE_FILE_PATH)
    else:
        log.info('Will save the result in Opensearch, host: %s - port: %s' % (settings.ES_HOST, settings.ES_PORT))

    cached_langcodes = load_cached_langcodes()

    enrich_occupations()

    enriched_occupations_to_store = filter_and_add_relevant_enriched_candidates(list(enriched_aggregated_occupations.values()))

    log.info(f'Found {len(enriched_occupations_to_store)} unique occupations that have been enriched.')

    if settings.OCCUPATIONS_SAVE_RESULT_IN_FILE_ONLY:
        complete_file_path = currentdir + settings.OCCUPATIONS_RELATIVE_FILE_PATH
        log.info('Writing enriched occupations to file: %s' % complete_file_path)
        write_items_to_file(enriched_occupations_to_store, complete_file_path)
    else:
        save_enriched_occupations_in_repository(enriched_occupations_to_store)

# enrich_occupations_and_persist()
