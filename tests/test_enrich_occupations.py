import ndjson
import pytest
from educationenrichoccupations.enrich_occupations import *

log = logging.getLogger(__name__)
currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'


@pytest.fixture(scope="module")
def historical_ads():
    return load_ndjson_file(currentdir + 'resources/historical_ads_2020_beta1_1_percent_ensure_ascii_part_1_of_4.jsonl')


# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_doc_headline_input(historical_ads):
    test_ad = get_test_ad_by_id('24433095', historical_ads)
    headline_input = get_doc_headline_input(test_ad)
    # print(headline_input)
    # Make sure that the occupation label was found in the json-structure.
    assert headline_input.lower().startswith('sjuksköterska, grundutbildad')


# @pytest.mark.skip(reason="Temporarily disabled")
def test_enrich_and_append_to_occupations(historical_ads):
    test_ad = get_test_ad_by_id('23981973', historical_ads)
    enrich_and_append_to_occupations([test_ad])
    # print(json.dumps(test_ad, indent=4))
    assert 'text_enrichments_results' in test_ad
    assert 'enriched_candidates' in test_ad['text_enrichments_results']
    candidates = jmespath.search('text_enrichments_results.enriched_candidates', test_ad)
    assert 'occupations' in candidates
    assert 'competencies' in candidates
    assert len(candidates['competencies']) > 0


# @pytest.mark.skip(reason="Temporarily disabled")
def test_enrich_and_append_deprecated_occupation_to_occupations(historical_ads):
    test_ad = get_test_ad_by_id('23740934', historical_ads)
    enrich_and_append_to_occupations([test_ad])
    # print(json.dumps(test_ad, indent=4))
    assert 'text_enrichments_results' in test_ad
    assert 'enriched_candidates' in test_ad['text_enrichments_results']
    candidates = jmespath.search('text_enrichments_results.enriched_candidates', test_ad)
    assert 'occupations' in candidates
    assert 'competencies' in candidates
    assert len(candidates['competencies']) > 0

# @pytest.mark.skip(reason="Temporarily disabled")
def test_enrich_and_append_deprecated_occupation_with_multiple_replaced_by_to_occupations(historical_ads):
    test_ad = get_test_ad_by_id('0017-90616', historical_ads)
    # occupation for ad 0017-90616: {"concept_id": "YrxV_b6A_Dav", "label": "Annonssekreterare", "legacy_ams_taxonomy_id": "3727"}

    enrich_and_append_to_occupations([test_ad])
    # print(json.dumps(test_ad, indent=4))

    concept_id_marknadsassistent = 'uWwq_jF2_nhq'
    assert jmespath.search("occupation.concept_id", test_ad) == concept_id_marknadsassistent
    concept_id_occupation_group_marknads_och_forsaljningsassistenter = 'p16X_44f_rwZ'
    assert jmespath.search("occupation_group.concept_id", test_ad) == concept_id_occupation_group_marknads_och_forsaljningsassistenter
    concept_id_occupation_field_forsaljning_mm = 'RPTn_bxG_ExZ'
    assert jmespath.search("occupation_field.concept_id", test_ad) == concept_id_occupation_field_forsaljning_mm

    candidates = jmespath.search('text_enrichments_results.enriched_candidates', test_ad)
    assert candidates
    assert 'occupations' in candidates
    assert 'competencies' in candidates
    assert len(candidates['competencies']) > 0

# @pytest.mark.skip(reason="Temporarily disabled")
def test_enrich_and_append_deprecated_occupation_with_same_replaced_by_to_occupations(historical_ads):
    test_ad = get_test_ad_by_id('0017-546180', historical_ads)
    #  occupation data for ad 0017-546180:
    # "occupation": {"concept_id": "Wmuh_RXD_cv5", "label": "Grafisk operat\u00f6r, digital produktion", "legacy_ams_taxonomy_id": "7376"},
    # "occupation_field": {"concept_id": "wTEr_CBC_bqh", "label": "Industriell tillverkning", "legacy_ams_taxonomy_id": "9"},
    # "occupation_group": {"concept_id": "LeQV_umf_wUS", "label": "Prepresstekniker", "legacy_ams_taxonomy_id": "7321"}

    enrich_and_append_to_occupations([test_ad])
    # print(json.dumps(test_ad, indent=4))

    concept_id_grafisk_operator = 'Wmuh_RXD_cv5'
    assert jmespath.search("occupation.concept_id", test_ad) == concept_id_grafisk_operator
    concept_id_occupation_group_prepresstekniker = 'LeQV_umf_wUS'
    assert jmespath.search("occupation_group.concept_id", test_ad) == concept_id_occupation_group_prepresstekniker
    concept_id_occupation_field_industriell_tillverkning = 'wTEr_CBC_bqh'
    assert jmespath.search("occupation_field.concept_id", test_ad) == concept_id_occupation_field_industriell_tillverkning

    candidates = jmespath.search('text_enrichments_results.enriched_candidates', test_ad)
    assert candidates
    assert 'occupations' in candidates
    assert 'competencies' in candidates
    # Note: This ad should not lead to enriched candidates, since it has an old occupation that doesn't point to a
    # newer/valid occupation.
    assert len(candidates['competencies']) == 0



# @pytest.mark.skip(reason="Temporarily disabled")
def test_get_doc_headline_input2(historical_ads):
    test_ad = get_test_ad_by_id('23981973', historical_ads)
    headline_input = get_doc_headline_input(test_ad)
    # print(headline_input)
    # Make sure that the occupation label was found in the json-structure.
    assert headline_input.lower().startswith('grundlärare, 4-6')


def get_test_ad_by_id(id, dicts):
    return next((item for item in dicts if item["id"] == id), None)


def load_ndjson_file(filepath):
    print('Loading json from file: %s' % filepath)
    with open(filepath, 'r', encoding='utf-8') as file:
        data = ndjson.load(file)
        return data

if __name__ == '__main__':
    pytest.main([os.path.realpath(__file__), '-svv', '-ra'])
