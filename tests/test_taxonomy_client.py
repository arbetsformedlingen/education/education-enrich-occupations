import pytest

from educationenrichoccupations.enrich_occupations import *

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'

log = logging.getLogger(__name__)


@pytest.fixture(scope="module")
def taxonomy_client():
    return TaxonomyClient()


# @pytest.mark.skip(reason="Temporarily disabled")
def test_load_taxonomy_occupations(taxonomy_client):
    taxonomy_occupations = taxonomy_client.load_taxonomy_occupations()
    assert len(taxonomy_occupations) > 0

    for taxonomy_occupation in taxonomy_occupations:
        assert jmespath.search('id', taxonomy_occupation)
        assert jmespath.search('preferred_label', taxonomy_occupation)


# @pytest.mark.skip(reason="Temporarily disabled")
def test_load_occupation_id_to_occupation_group(taxonomy_client):
    id_to_taxonomy_occupation = taxonomy_client.load_occupation_id_to_taxonomy_occupation()
    assert len(id_to_taxonomy_occupation) > 0

    for id, taxonomy_occupation in id_to_taxonomy_occupation.items():
        assert jmespath.search('concept_id', taxonomy_occupation)
        assert jmespath.search('label', taxonomy_occupation)

        if not jmespath.search('occupation_group', taxonomy_occupation):
            assert jmespath.search('replaced_by', taxonomy_occupation)
        else:
            assert jmespath.search('occupation_group.concept_id', taxonomy_occupation)
            assert jmespath.search('occupation_group.label', taxonomy_occupation)



# @pytest.mark.skip(reason="Temporarily disabled")
def test_load_current_taxonomy_occupations(taxonomy_client):
    current_taxonomy_occupations = taxonomy_client.load_current_taxonomy_occupations()

    assert len(current_taxonomy_occupations) > 0

    for taxonomy_occupation in current_taxonomy_occupations:
        assert jmespath.search('id', taxonomy_occupation)
        assert jmespath.search('preferred_label', taxonomy_occupation)

# @pytest.mark.skip(reason="Temporarily disabled")
def test_load_current_taxonomy_occupation_ids(taxonomy_client):
    taxonomy_occupation_ids = taxonomy_client.load_current_taxonomy_occupation_ids()

    assert len(taxonomy_occupation_ids) > 0



if __name__ == '__main__':
    pytest.main([os.path.realpath(__file__), '-svv', '-ra'])
