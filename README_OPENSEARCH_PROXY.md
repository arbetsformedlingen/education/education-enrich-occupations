# Setup instructions in Windows for reaching Opensearch in production

## Oc client
- Copy oc-client to your local machine (ask Calamari if you don't have the oc.exe)
- Go to kontrollpanelen and choose "Redigera systemets miljövariabler". 
- Click on "Miljövariabler"
- In "Systemvariabler", add the local folder path for oc.exe in the variable "Path", so oc.exe can be found from a terminal

## Start the proxy
- Clone https://gitlab.com/arbetsformedlingen/devops/https-proxy-in-openshift
- Login to Openshift and Copy login command
- Open 1st Git bash terminal window and login with the login command (if you get a warning about the certificate, add additional flag --insecure-skip-tls-verify)
- Choose Openshift project to start the proxy in, for example: 
``` 
oc project education-scraping-pipeline-prod  
```
- cd to local folder with the file create-https-proxy-in-openshift.sh, for example: cd /c/data/git/https-proxy-in-openshift
- Run the script 
```
bash create-https-proxy-in-openshift.sh
```
- Wait for message "now please run: oc port-forward pod/stupid-proxy 3128:3128"
- Open a 2nd Git bash terminal window as Administrator and enter:  
```
oc port-forward pod/stupid-proxy 3128:3128
```
**Warning:** If two proxies are started simultaneous (by two persons or more), the last created proxy pod (with name "stupid-proxy") will destroy the first proxy and disconnect person 1. 

### Surf to dashboards (proxy must be started at this point)
- Start Firefox
- Open menu option "Settings" and search for "Network Settings"
- Click "Manual proxy configuration" and enter:
```
HTTPS Proxy: localhost
Port: 3128
```
- Click Ok and surf to:
  https://education-prod-opensearch.jobtechdev.se/_dashboards


### Upload data from education-enrich-occupations to Opensearch in production

#### Configure settings in project education-opensearch
education-enrich-occupations/educationenrichoccupations/settings.py
```
os.environ['https_proxy'] = "http://localhost:3128"
ES_HOST = "education-prod-opensearch.jobtechdev.se"
ES_PORT = 443
ES_USE_SSL = True
ES_VERIFY_CERTS = False
ES_USER = "your_username"
ES_PWD = "your password"
```
Note: "education-prod-opensearch.jobtechdev.se" is an alias to
"http://vpc-education-prod-24hdczerfmb54sqmlu3joyguua.eu-north-1.es.amazonaws.com"
since the address mustn't be longer than 64 characters for the proxy .

#### Upload data to Opensearch in production
- Verify settings in your local file  
  education-enrich-occupations\educationenrichoccupations\settings.py
  ...so it has the correct setting for OCCUPATIONS_RELATIVE_FILE_PATH (the file to upload)
- Open a 3rd window with Git bash
- Activate your virtual python environment, for example:
conda activate education
- Navigate to your local folder for the file
  education-enrich-occupations/tools/persist_jsonfile_occupations_in_opensearch.py  
...for example: cd /c/data/git/education-enrich-occupations/tools
- Run python script to upload the data:   
```
python persist_jsonfile_occupations_in_opensearch.py
```

