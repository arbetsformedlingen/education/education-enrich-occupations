import json
import logging
import os
import gzip

import jmespath

from educationenrichoccupations import settings
from educationenrichoccupations.helpers import detect_langcode
from educationenrichoccupations.local_file_downloader import LocalFileDownloader
from educationenrichoccupations.taxonomy_client import TaxonomyClient

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'
taxonomy_client = TaxonomyClient()
occupation_id_to_taxonomy_occupation = taxonomy_client.load_occupation_id_to_taxonomy_occupation()

FILEPATH_JOBAD_ID_2_LANGCODE_2020_2024 = currentdir + '../educationenrichoccupations/resources/jobad_id_lang_code_cache_2020_2024.json.gz'


def load_json_gz_file(filepath):
    with gzip.open(filepath, 'rt', encoding='utf-8') as f:
        data = json.load(f)
        return data

def save_json_gz_file(data, filepath):
    log.info('Saving cached langcodes to file: %s' % filepath)
    with gzip.open(filepath, 'wt', encoding='utf-8') as f:
        json.dump(data, f)


def load_cached_langcodes(filepath):
    cached_lang_codes = load_json_gz_file(filepath)
    log.info('Loaded %s cached langcodes' % len(cached_lang_codes))

    return cached_lang_codes

def save_cached_langcodes(cached_langcodes, filepath):
    save_json_gz_file(cached_langcodes, filepath)

def get_occupation_from_ad(ad):
    occupation_path = jmespath.search("occupation", ad)
    occupation = {}
    if isinstance(occupation_path, dict):
        occupation = occupation_path
    elif isinstance(occupation_path, list):
        occupation = jmespath.search("occupation[0]", ad)
    else:
        log.warning(f"Could not find occupation in ad {ad['id']}")
    return occupation

def create_language_cache_file():


    #  Iterate all ads between 2020-2024. Append language code for each ad in a dict that
    #  will be saved in jobad_id_lang_code_cache_2020_2024.json.
    local_file_downloader = LocalFileDownloader()
    ad_src_file_names = local_file_downloader.get_filtered_filenames()
    log.info(f'Will use local files in {settings.AD_SRC_RELATIVE_LOCAL_FILES_PATH} '
             f'and load ads from the following files:\n{ad_src_file_names}')

    ad_counter = 0

    cached_langcodes = load_cached_langcodes(FILEPATH_JOBAD_ID_2_LANGCODE_2020_2024)
    print(f'Loaded {len(cached_langcodes)} langcodes')

    for ad_src_file_name in ad_src_file_names:
        log.info(f'Loading ads from file: {ad_src_file_name}')

        for ad in local_file_downloader.download_ads_from_file(ad_src_file_name):
            # Exlude ads that have a deprecated occupation in Taxonomy
            occupation = get_occupation_from_ad(ad)

            occupation_id = jmespath.search("concept_id", occupation)

            if not occupation_id in occupation_id_to_taxonomy_occupation:
                continue

            doc_id = str(ad.get('id', ''))
            doc_text = ad.get('description', {}).get('text_formatted', '')

            if not doc_text:
                doc_text = ad.get('description', {}).get('text', '')

            if doc_id not in cached_langcodes:
                # Check language, only enrich for swedish ads.
                text_for_detection = None
                if doc_text:
                    text_for_detection = doc_text.strip()[:400]

                detected_lang_code = detect_langcode(text_for_detection)
                # log.info('Detected lang_code for ad id %s: %s' % (doc_id, detected_lang_code))
                cached_langcodes[doc_id] = detected_lang_code

            ad_counter += 1
            if ad_counter % 1000 == 0:
                log.info('Processed ads, so far: %s' % ad_counter)

            if ad_counter % 100000 == 0 and ad_counter >= len(cached_langcodes):
                save_cached_langcodes(cached_langcodes, FILEPATH_JOBAD_ID_2_LANGCODE_2020_2024)

    log.info(f'Handled {len(cached_langcodes)} langcodes')
    save_cached_langcodes(cached_langcodes, FILEPATH_JOBAD_ID_2_LANGCODE_2020_2024)






create_language_cache_file()