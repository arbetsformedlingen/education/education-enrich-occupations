import json
import math
import os

from educationenrichoccupations.helpers import grouper

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'



# source_file = currentdir + 'ads_resources/2019.jsonl'
# dest_file = currentdir + 'files_to_upload_to_minio/historical_ads_2019_ensure_ascii_part_%s_of_%s.jsonl'

# source_file = currentdir + 'ads_resources/2020.jsonl'
# dest_file = currentdir + 'files_to_upload_to_minio/historical_ads_2020_ensure_ascii_part_%s_of_%s.jsonl'

# source_file = currentdir + 'ads_resources/2021.jsonl'
# dest_file = currentdir + 'files_to_upload_to_minio/historical_ads_2021_ensure_ascii_part_%s_of_%s.jsonl'

# source_file = currentdir + 'ads_resources/2022.jsonl'
# dest_file = currentdir + 'files_to_upload_to_minio/historical_ads_2022_ensure_ascii_part_%s_of_%s.jsonl'

# source_file = currentdir + 'ads_resources/2023.jsonl'
# dest_file = currentdir + 'files_to_upload_to_minio/historical_ads_2023_ensure_ascii_part_%s_of_%s.jsonl'

source_file = currentdir + 'ads_resources/2024.jsonl'
dest_file = currentdir + 'files_to_upload_to_minio/historical_ads_2024_ensure_ascii_part_%s_of_%s.jsonl'

def load_ndjson_file_lines(filepath):
    counter = 0
    with open(filepath, 'r', encoding='utf-8') as file:
        for line in file:
            counter += 1
            if counter % 10000 == 0:
                print(f"Read {counter} lines")
            yield json.loads(line)

def write_records_to_file(json_src, filepath):
    print('Writing records to file: %s' % filepath)
    file = open(filepath, 'w', encoding='utf-8')
    for record in json_src:
        file.write((json.dumps(record, ensure_ascii=True)).strip() + '\n')
    file.close()


def convert_historical_ads_to_ndjson_parts():
    json_src = []
    for record in load_ndjson_file_lines(source_file):
        json_src.append(record)
    print("Done reading file")

    historical_ads_size = len(json_src)

    nr_of_parts = 4


    nr_of_items_per_batch = math.ceil(historical_ads_size / nr_of_parts)

    ad_batches = grouper(nr_of_items_per_batch, json_src)

    for i, batch in enumerate(ad_batches):
        dest_file_part = dest_file % ((i + 1), nr_of_parts)
        # print(dest_file_part)
        # print(len(batch))
        write_records_to_file(batch, dest_file_part)

## Convert ads file in json format into a ndjson file.
convert_historical_ads_to_ndjson_parts()
