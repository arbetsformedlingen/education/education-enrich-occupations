import json
import logging
import os

import jmespath
import ndjson

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'

PATH_FILE1 = currentdir + '../educationenrichoccupations/enriched-occupations-2017-2022-synonymdic-2.0.1.308-tax-v21.jsonl'
PATH_FILE2 = currentdir + '../educationenrichoccupations/enriched-occupations-2020-2024-synonymdic-2.0.1.308-tax-v21.jsonl'

def load_ndjson_file(filepath):
    log.info(f'Loading json from file: {filepath}' )
    with open(filepath, 'r', encoding='utf-8') as file:
        data = ndjson.load(file)
        return data

def write_items_to_file(json_src, filepath):
    log.info(f'Saving file: {filepath}' )
    file = open(filepath, 'w', encoding='utf-8')
    for record in json_src:
        file.write(json.dumps(record) + '\n')
    file.close()


def create_and_sort_slim_dict(enriched_occupations):
    occupations_slim = []
    for occupation in enriched_occupations:
        occupation_data = jmespath.search('occupation', occupation)
        if occupation_data:
            label = jmespath.search('label', occupation_data)
            concept_id = jmespath.search('concept_id', occupation_data)

            slim_data = { 'label': label, 'concept_id': concept_id }
            occupations_slim.append(slim_data)

    occupations_slim.sort(key=lambda x: x['label'])

    return occupations_slim

def compare_files():
    enriched_occupations1 = load_ndjson_file(PATH_FILE1)

    enriched_occupations1_slim = create_and_sort_slim_dict(enriched_occupations1)

    write_items_to_file(enriched_occupations1_slim, 'enriched_occupations_slim_2017-2022.jsonl')
    enriched_occupations2 = load_ndjson_file(PATH_FILE2)
    enriched_occupations2_slim = create_and_sort_slim_dict(enriched_occupations2)
    write_items_to_file(enriched_occupations2_slim, 'enriched_occupations_slim_2020-2024.jsonl')

    diff_only_in_occupations1 = [item for item in enriched_occupations1_slim if item not in enriched_occupations2_slim]
    log.info(f"In occupations1 but not in occupations2 (size: {len(diff_only_in_occupations1)}): {diff_only_in_occupations1}")
    diff_only_in_occupations2 = [item for item in enriched_occupations2_slim if item not in enriched_occupations1_slim]
    log.info(f"In occupations2 but not in occupations1 (size: {len(diff_only_in_occupations2)}): {diff_only_in_occupations2}")




compare_files()