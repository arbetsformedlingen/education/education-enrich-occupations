import os
import json

from educationenrichoccupations.helpers import load_json_file

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'

# source_file = currentdir + 'ads_resources/historical_ads_sample_2021_first_6_months.json'
# dest_file = currentdir + 'ads_resources/historical_ads_sample_2021_first_6_months.jsonl'

# source_file = currentdir + 'ads_resources/historical_ads_2020.json'
# dest_file = currentdir + 'ads_resources/historical_ads_2020.jsonl'

source_file = currentdir + 'ads_resources/historical_ads_2021_first_6_months.json'
dest_file = currentdir + 'ads_resources/historical_ads_2021_first_6_months.jsonl'


def write_records_to_file(json_src, filepath):
    print('Writing records to file: %s' % filepath)
    file = open(filepath,'w', encoding='utf-8')
    for record in json_src:
        file.write(json.dumps(record) + '\n')
    file.close()

json_src = load_json_file(source_file)
write_records_to_file(json_src, dest_file)



