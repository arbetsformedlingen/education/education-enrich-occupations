import os
import json
currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'

source_file_1 = currentdir + 'ads_resources/2022q1.json'
source_file_2 = currentdir + 'ads_resources/2022q2.json'
source_file_3 = currentdir + 'ads_resources/2022q3.json'
dest_file = currentdir + 'ads_resources/2022.jsonl'

def load_json_file(filepaths):
    complete_data = []
    for filepath in filepaths:
        print('Loading json from file: %s' % filepath)
        with open(filepath, 'r', encoding='utf-8') as file:
            data = json.load(file)
            complete_data.extend(data)
    return complete_data



def write_records_to_file(json_src, filepath):
    print('Writing records to file: %s' % filepath)
    file = open(filepath,'w', encoding='utf-8')
    for record in json_src:
        file.write(json.dumps(record) + '\n')
    file.close()

## Convert ads files in json format where same year is represented in multiple files (e.g. 20xxq1.json).
## The ads are stored in a single ndjson file.
json_src = load_json_file(filepaths=[source_file_1, source_file_2, source_file_3])
write_records_to_file(json_src, dest_file)



