import copy
import json
import ntpath
import os
import ndjson

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'


def load_json_file(filepath):
    print('Loading json from file: %s' % filepath)
    with open(filepath, 'r', encoding='utf-8') as file:
        data = json.load(file)
        return data

def load_ndjson_file(filepath):
    print('Loading jsonl from file: %s' % filepath)
    with open(filepath, 'r', encoding='utf-8') as file:
        reader = ndjson.reader(file)

        for post in reader:
            yield post


def remove_values_recursive(dictionary):
    for key, value in dictionary.items():
        if type(value) is dict:
            # yield from remove_values_recursive(value)
            remove_values_recursive(value)
        elif type(value) is list:
            for counter, item in enumerate(value):
                if type(item) is dict:
                    remove_values_recursive(item)
                    # print('Dict key: %s, calling recursive' % (key))

                else:
                    # print('Setting list item to %s' % (str(type(item))))
                    value[counter] = str(type(item))
        else:
            dictionary[key] = str(type(value))
            # print('Setting key: %s to %s' % (key, str(type(value))))


def remove_values_recursive_shallow(dictionary):
    for key, value in dictionary.items():
        if type(value) is dict:
            # print('Dict key: %s, calling recursive' % (key))
            remove_values_recursive_shallow(value)
        elif type(value) is list:
            if value:
                item = copy.deepcopy(value[0])
                value = []
                value.append(item)
                dictionary[key] = value
                if type(item) is dict:
                    # print('Dict key in list item 0: %s, calling recursive' % (key))
                    remove_values_recursive_shallow(item)
                else:
                    # print('Setting list item to %s' % (str(type(item))))
                    value[0] = f'{type(value).__name__} [{type(item).__name__}]'
            else:
                dictionary[key] = f'{type(value).__name__} [Empty]'
        else:
            # print('Setting key: %s to %s' % (key, str(type(value))))
            dictionary[key] = type(value).__name__


def merge(a, b, path=None):
    "merges b into a"
    if path is None: path = []
    for key in b:
        if key in a:
            if isinstance(a[key], dict) and isinstance(b[key], dict):
                merge(a[key], b[key], path + [str(key)])
            elif isinstance(a[key], list) and isinstance(b[key], list):
                if isinstance(a[key][0], dict) and isinstance(b[key][0], dict):
                    merge(a[key][0], b[key][0], path + [str(key)])
            elif a[key] == b[key]:
                pass  # same leaf value
            elif a[key] != type(None) and b[key]:
                a[key] = b[key]
            # else:
            #     raise Exception('Conflict at %s' % '.'.join(path + [str(key)]))
        else:
            # print('Adding missing key: %s' % key)
            a[key] = b[key]
    return a

def write_json_to_file(json_src, filepath):
    with open(filepath,'w', encoding='utf-8') as file:
        file.write(json.dumps(json_src, indent=4, sort_keys=True))


def print_json_structure(ads_filepath, max_ads_to_check=100):
    print(f'Loading file {ads_filepath} for json structure')
    if ads_filepath.endswith('.jsonl'):
        json_data = load_ndjson_file(ads_filepath)
    else:
        json_data = load_json_file(ads_filepath)

    # nr_of_items = len(json_data)
    # nr_of_items = min(max_ads_to_check, len(json_data))

    from collections import OrderedDict
    item_merged = OrderedDict()
    counter = 0
    for item in json_data:
        itemcopy = copy.deepcopy(item)
        # remove_values_recursive(itemcopy)
        # pprint(itemcopy)
        remove_values_recursive_shallow(itemcopy)
        # print(('*' * 40) + '\nitemcopy\n' + ('-' * 40))
        # print(json.dumps(itemcopy))
        # print(('-' * 40) + '\nitemmerged\n' + ('-' * 40))
        item_merged = merge(itemcopy, item_merged)
        # print(json.dumps(item_merged))
        counter += 1
        if counter != 0 and counter % 1000 == 0:
            print(f'Processed {counter} items')
        if counter >= max_ads_to_check:
            print(f'Processed enough items, max_ads_to_check: {max_ads_to_check}')
            break

    print(('*' * 40) + '\njson structure\n' + ('*' * 40))
    print(json.dumps(item_merged, indent=4))

    original_filename = ntpath.basename(ads_filepath)
    if ads_filepath.endswith('.jsonl'):
        new_filename = original_filename.replace('.jsonl', f'_json_structure_{counter}.json')
    else:
        new_filename = original_filename.replace('.json', f'_json_structure_{counter}.json')


    structure_folder_suffix = 'json_structure'
    curr_dir_path = ntpath.dirname(base_filepath)
    new_dir_path = f'{curr_dir_path}_{structure_folder_suffix}'
    if not os.path.isdir(new_dir_path):
        os.mkdir(new_dir_path)

    filepath_write = f'{new_dir_path}/{new_filename}'
    # filepath_write = base_path + new_filename
    write_json_to_file(item_merged, filepath_write)
    print(f'File saved as {filepath_write}')

#
filenames = [
    'historical_ads_2017.jsonl',
    'historical_ads_2018.jsonl',
    'historical_ads_2019.jsonl',
    'historical_ads_2020.jsonl',
    'historical_ads_2021.jsonl',
    'historical_ads_2022.jsonl',
]

# filenames = [
    # 'historical_ads_2016.enriched.jsonl',
    # 'historical_ads_2017.enriched.jsonl',
    # 'historical_ads_2018.enriched.jsonl',
    # 'historical_ads_2019.enriched.jsonl',
    # 'historical_ads_2020.enriched.jsonl',
    # 'historical_ads_2021.enriched.jsonl',
    # 'historical_ads_2022.enriched.jsonl',
    # 'historical_ads_2023.enriched.jsonl',
# ]

# filenames = [
#     'historical_ads_2016_enriched.sample.jsonl',
#     'historical_ads_2017_enriched.sample.jsonl',
#     'historical_ads_2018_enriched.sample.jsonl',
#     'historical_ads_2019_enriched.sample.jsonl',
#     'historical_ads_2020_enriched.sample.jsonl',
#     'historical_ads_2021_enriched.sample.jsonl',
#     'historical_ads_2022q1_enriched.sample.jsonl',
#     'historical_ads_2022q2_enriched.sample.jsonl',
#     # 'historical_ads_2023.enrichedsample20.jsonl',
#     # 'historical_ads_2023.enrichedsample2000.jsonl',
# ]
base_filepath = currentdir + 'original_historical_ads/'
# base_filepath = currentdir + 'original_historical_enriched_ads/'


for filename in filenames:
    ads_filepath = base_filepath + filename
    print_json_structure(ads_filepath, max_ads_to_check=100000)

