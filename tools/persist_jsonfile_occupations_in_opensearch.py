import logging
import os

import ndjson
from opensearchpy import OpenSearch, RequestsHttpConnection
from education_opensearch.opensearch_store import OpensearchStore

from educationenrichoccupations import settings
from educationenrichoccupations.enrich_occupations import filter_and_add_relevant_enriched_candidates

logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'

def create_os_client():
    auth = (settings.ES_USER, settings.ES_PWD)
    log.info(f"Creating client for opensearch host {settings.ES_HOST} {settings.ES_PORT} with user {settings.ES_USER} use_ssl = {settings.ES_USE_SSL} verify certs = {settings.ES_VERIFY_CERTS}")
    os_client = OpenSearch(
        hosts = [{'host': settings.ES_HOST, 'port': settings.ES_PORT}],
        http_compress = True, # enables gzip compression for request bodies
        http_auth = auth,
        use_ssl = settings.ES_USE_SSL,
        verify_certs = settings.ES_VERIFY_CERTS,
        ssl_assert_hostname = False,
        ssl_show_warn = False,
        timeout=30,
        max_retries=10,
        retry_on_timeout=True,
        connection_class=RequestsHttpConnection
    )
    return os_client


def load_ndjson_file_stream(filepath):
    log.info('Loading jsonl from file: %s' % filepath)
    counter = 0
    with open(filepath, 'r', encoding='utf-8') as file:
        reader = ndjson.reader(file)
        for post in reader:
            counter += 1
            if counter % 100 == 0:
                log.info(f'Loaded {counter} occupations from {filepath}')
            yield post


def save_enriched_occupations_from_file_in_repository():
    enriched_aggregated_occupations = list(load_ndjson_file_stream(currentdir + settings.OCCUPATIONS_RELATIVE_FILE_PATH))

    log.info(f"Loaded {len(enriched_aggregated_occupations)} occupations from file.")
    log.info(f"Starting to filter and add relevant enriched candidates..")
    enriched_occupations_to_store = filter_and_add_relevant_enriched_candidates(enriched_aggregated_occupations)

    save_in_repository(enriched_occupations_to_store)


def save_in_repository(enriched_occupations_to_store):
    log.info(f"Saving {len(enriched_occupations_to_store)} occupations in repository")
    os_client = create_os_client()
    os_store = OpensearchStore(os_client=os_client)

    index_name = os_store.start_new_save(settings.OCCUPATIONS_ALIAS_NAME,
                                         mappings=settings.ENRICHED_OCCUPATIONS_INDEX_CONFIG)
    log.info('Will save enriched occupations in index_name: %s' % index_name)
    os_store.save_items_in_repository(enriched_occupations_to_store)
    os_store.create_or_update_alias_for_index(index_name, settings.OCCUPATIONS_ALIAS_NAME)


save_enriched_occupations_from_file_in_repository()
